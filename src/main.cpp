#include <QtGui/QApplication>
#include "core/appcontroller.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    AppController ac;
    ac.exec();
    return app.exec();
}

