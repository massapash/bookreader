#include "gui/bookwindow.h"
#include <QtGui/QFileDialog>
#include <QtGui/QMessageBox>
#include <QtGui/QPalette>
#include <QtGui/QScrollBar>

BookWindow::BookWindow(QWidget *parent)
    : QMainWindow(parent), m_ui(new Ui::BookWindow)
{
    m_ui->setupUi(this);
    createForms();
    createActions();
    connectSignals();
}

BookWindow::~BookWindow()
{
    delete m_actionExitApp;
    delete m_actionShowActions;
    delete m_contentsDialog;
    delete m_configForm;
    delete m_ui;
}

void BookWindow::showBook(const QString &textString)
{
    m_ui->bookBrowser->setText(textString);
    enableControls();
}

void BookWindow::showContents(const QStringList &contentsList)
{
    m_contentsDialog->showContents(contentsList);
}

void BookWindow::applyConfig(const Configuration &config)
{
    QPalette palette;
    palette = this->palette();
    palette.setColor(QPalette::Window, config.background);
    this->setPalette(palette);
    palette = m_ui->bookBrowser->palette();
    palette.setColor(QPalette::Text, config.text);
    m_ui->bookBrowser->setPalette(palette);
    m_ui->bookBrowser->setFont(config.font);
    m_config = config;
}

Configuration BookWindow::getConfig() const
{
    return m_config;
}

void BookWindow::openBook()
{
    disableAutoScroll();
    QString fileName = QFileDialog::getOpenFileName();
    emit openBookTriggered(fileName);
}

void BookWindow::findText()
{
    disableAutoScroll();
    if (!m_searchDialog) {
        m_searchDialog = new SearchDialog();
        connect(m_searchDialog, SIGNAL(searchTriggered(QString,QTextDocument::FindFlags)),
                this, SLOT(search(QString,QTextDocument::FindFlags)));
    }
    m_searchDialog->show();
}

void BookWindow::addMark()
{
    QString selection = m_ui->bookBrowser->textCursor().selectedText();
    if (selection.length() < 50) {
        QMessageBox::information(this, tr("Information"), tr("Select at least 50 symbols for quote"));
    } else {
        selection.truncate(50);
        emit saveMarkTriggered(selection);
    }
}

void BookWindow::showMarks()
{
    disableAutoScroll();
    if (!m_contentsDialog) {
        m_contentsDialog = new ContentsDialog();
        connect(m_contentsDialog, SIGNAL(markSelectionTriggered(QString)),
                this, SLOT(search(QString)));
    }
    emit loadContentsTriggered();
    m_contentsDialog->show();
}

void BookWindow::goBack()
{
    disableAutoScroll();
    //TODO implement this feature later
    m_ui->bookBrowser->backward();
}

void BookWindow::autoScroll(bool isActive)
{
    if (isActive)
        m_timer = startTimer(m_config.delay);
    else
        killTimer(m_timer);
}

void BookWindow::fullScreen(bool isActive)
{
    if (isActive) {
        showFullScreen();
        m_ui->actionsBar->hide();
    } else {
        showNormal();
    }
}

void BookWindow::showOptions()
{
    disableAutoScroll();
    if (!m_configForm) {
        m_configForm = new ConfigForm();
        connect(m_configForm, SIGNAL(applyConfigTriggered(Configuration)),
                this, SLOT(saveConfig(Configuration)));
    }
    m_configForm->showConfig(m_config);
}

void BookWindow::showActions()
{
    m_ui->actionsBar->show();
}

void BookWindow::enableAddMark()
{
    QString selection = m_ui->bookBrowser->textCursor().selectedText();
    if (selection.length() > 0)
        m_ui->actionAddMark->setEnabled(true);
    else
        m_ui->actionAddMark->setEnabled(false);
}

void BookWindow::search(const QString &keyWord, QTextDocument::FindFlags searchFlags)
{
    if (m_ui->bookBrowser->find(keyWord, searchFlags)) {
        return;
    } else {
        searchFlags |= QTextDocument::FindBackward;
        m_ui->bookBrowser->find(keyWord, searchFlags);
    }
}

void BookWindow::saveConfig(const Configuration &newConfig)
{
    applyConfig(newConfig);
    emit saveConfigTriggered();
}

void BookWindow::connectSignals() const
{
    connect(m_ui->actionOpenBook, SIGNAL(triggered()), this, SLOT(openBook()));
    connect(m_ui->actionFindtext, SIGNAL(triggered()), this, SLOT(findText()));
    connect(m_ui->actionAddMark, SIGNAL(triggered()), this, SLOT(addMark()));
    connect(m_ui->actionShowMarks, SIGNAL(triggered()), this, SLOT(showMarks()));
    connect(m_ui->actionGoBack, SIGNAL(triggered()), this, SLOT(goBack()));
    connect(m_ui->actionAutoScroll, SIGNAL(triggered(bool)), this, SLOT(autoScroll(bool)));
    connect(m_ui->actionFullScreen, SIGNAL(triggered(bool)), this, SLOT(fullScreen(bool)));
    connect(m_ui->actionOptions, SIGNAL(triggered()), this, SLOT(showOptions()));
    connect(m_actionExitApp, SIGNAL(triggered()), this, SLOT(close()));
    connect(m_actionShowActions, SIGNAL(triggered()), this, SLOT(showActions()));
    connect(m_ui->bookBrowser, SIGNAL(selectionChanged()), this, SLOT(enableAddMark()));
    connect(m_ui->bookBrowser, SIGNAL(backwardAvailable(bool)),
            m_ui->actionGoBack, SLOT(setEnabled(bool)));

}

void BookWindow::createActions()
{
    m_actionExitApp = new QAction(tr("Exit"), this);
    m_actionExitApp->setShortcut(tr("Ctrl+Q"));
    addAction(m_actionExitApp);

    m_actionShowActions = new QAction(tr("Show"), this);
    m_actionShowActions->setShortcut(tr("Esc"));
    addAction(m_actionShowActions);
}

void BookWindow::createForms()
{
    m_searchDialog = 0;
    m_contentsDialog = 0;
    m_configForm = 0;
}

void BookWindow::enableControls()
{
    m_ui->actionAddMark->setEnabled(true);
    m_ui->actionFindtext->setEnabled(true);
    m_ui->actionShowMarks->setEnabled(true);
    m_ui->actionAutoScroll->setEnabled(true);
}

void BookWindow::disableAutoScroll()
{
    m_ui->actionAutoScroll->setChecked(false);
    killTimer(m_timer);
}

void BookWindow::timerEvent(QTimerEvent *event)
{
    Q_UNUSED(event);
    int step = m_ui->bookBrowser->verticalScrollBar()->singleStep();
    int value = m_ui->bookBrowser->verticalScrollBar()->value();
    m_ui->bookBrowser->verticalScrollBar()->setValue(value + step);
}

void BookWindow::showEvent(QShowEvent *)
{
    emit loadConfigTriggered();
}
