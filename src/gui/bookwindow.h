#ifndef BOOKWINDOW_H
#define BOOKWINDOW_H

#include "ui_bookwindow.h"
#include "gui/searchdialog.h"
#include "gui/configform.h"
#include "gui/contentsdialog.h"
#include "core/configuration.h"

class BookWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit BookWindow(QWidget *parent = 0);
    ~BookWindow();

    void showBook(const QString &textString);
    void showContents(const QStringList &contentsList);
    void applyConfig(const Configuration &config);
    Configuration getConfig() const;

signals:
    void openBookTriggered(const QString &fileName) const;
    void saveMarkTriggered(const QString &markQuote) const;
    void loadContentsTriggered() const;
    void loadConfigTriggered() const;
    void saveConfigTriggered() const;

private slots:
    void openBook();
    void findText();
    void addMark();
    void showMarks();
    void goBack();
    void autoScroll(bool isActive);
    void fullScreen(bool isActive);
    void showOptions();
    void showActions();
    void enableAddMark();

    void search(const QString &keyWord, QTextDocument::FindFlags searchFlags = 0);
    void saveConfig(const Configuration &newConfig);

private:
    void connectSignals() const;
    void createActions();
    void createForms();
    void enableControls();
    void disableAutoScroll();
    void timerEvent(QTimerEvent *event);
    void showEvent(QShowEvent *);

    Ui::BookWindow *m_ui;
    QAction *m_actionExitApp;
    QAction *m_actionShowActions;
    SearchDialog *m_searchDialog;
    ConfigForm *m_configForm;
    ContentsDialog *m_contentsDialog;
    int m_timer;
    Configuration m_config;
};

#endif // BOOKWINDOW_H
