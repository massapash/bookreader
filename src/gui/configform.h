#ifndef CONFIGFORM_H
#define CONFIGFORM_H

#include "ui_configform.h"
#include "core/configuration.h"

class ConfigForm : public QWidget
{
    Q_OBJECT
    
public:
    explicit ConfigForm(QWidget *parent = 0);
    void showConfig(const Configuration &config);

signals:
    void applyConfigTriggered(const Configuration &newConfig) const;

private slots:
    void updatePreview();
    void applyConfig();
    void restoreConfig();
    
private:
    void connectSignals();
    Ui::ConfigForm m_ui;
};

#endif // CONFIGFORM_H
