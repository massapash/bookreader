#ifndef SEARCHDIALOG_H
#define SEARCHDIALOG_H

#include "ui_searchdialog.h"
#include <QTextDocument>

class SearchDialog : public QWidget
{
    Q_OBJECT
    
public:
    explicit SearchDialog(QWidget *parent = 0);
    
signals:
    void searchTriggered(const QString, QTextDocument::FindFlags);

private slots:
    void searchForward();
    void searchBackward();

private:
    void connectSignals();

    Ui::SearchDialog m_ui;
};

#endif // SEARCHDIALOG_H
