#include "gui/contentsdialog.h"

ContentsDialog::ContentsDialog(QWidget *parent)
    : QWidget(parent)
{
    m_ui.setupUi(this);
    connectSignals();
}

void ContentsDialog::showContents(const QStringList &contentsList)
{
    m_ui.contentsListWidget->clear();
    m_ui.contentsListWidget->addItems(contentsList);
}

void ContentsDialog::markSelected(QListWidgetItem *bookmark)
{
    emit markSelectionTriggered(bookmark->text());
    close();
}

void ContentsDialog::connectSignals()
{
    connect(m_ui.contentsListWidget, SIGNAL(itemDoubleClicked(QListWidgetItem*)),
            this, SLOT(markSelected(QListWidgetItem*)));
}
