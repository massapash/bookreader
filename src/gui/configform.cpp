#include "gui/configform.h"

ConfigForm::ConfigForm(QWidget *parent)
    : QWidget(parent)
{
    m_ui.setupUi(this);
    connectSignals();
}

void ConfigForm::showConfig(const Configuration &config)
{
    m_ui.redSliderT->setValue(config.text.red());
    m_ui.greenSliderT->setValue(config.text.green());
    m_ui.blueSliderT->setValue(config.text.blue());
    m_ui.redSliderB->setValue(config.background.red());
    m_ui.greenSliderB->setValue(config.background.green());
    m_ui.blueSliderB->setValue(config.background.blue());
    m_ui.sizeSpinBox->setValue(config.font.pointSize());
    m_ui.fontComboBox->setCurrentFont(config.font);
    m_ui.autoscrollBox->setValue(config.delay);
    m_ui.texturePath->setText(config.texture);
    updatePreview();
    show();
}

void ConfigForm::updatePreview()
{
    QPalette palette;
    QColor color;
    QFont font;
    palette = m_ui.textPreview->palette();
    color.setRed(m_ui.redSliderT->value());
    color.setGreen(m_ui.greenSliderT->value());
    color.setBlue(m_ui.blueSliderT->value());
    palette.setColor(QPalette::Text, color);
    color.setRed(m_ui.redSliderB->value());
    color.setGreen(m_ui.greenSliderB->value());
    color.setBlue(m_ui.blueSliderB->value());
    palette.setColor(QPalette::Base, color);
    m_ui.textPreview->setPalette(palette);
    font.setFamily(m_ui.fontComboBox->currentFont().family());
    font.setPointSize(m_ui.sizeSpinBox->value());
    m_ui.textPreview->setFont(font);
    // font dont updates on preview
}

void ConfigForm::applyConfig()
{
    Configuration newConfig;
    int red;
    int green;
    int blue;
    QString texture;
    QString font;
    int size;
    int delay;

    red = m_ui.redSliderB->value();
    green = m_ui.greenSliderB->value();
    blue = m_ui.blueSliderB->value();
    newConfig.background.setRgb(red, green, blue);

    red = m_ui.redSliderT->value();
    green = m_ui.greenSliderT->value();
    blue = m_ui.blueSliderT->value();
    newConfig.text.setRgb(red, green, blue);

    texture = m_ui.texturePath->text();
    newConfig.texture = texture;

    font = m_ui.fontComboBox->currentFont().family();
    newConfig.font.setFamily(font);

    size = m_ui.sizeSpinBox->value();
    newConfig.font.setPointSize(size);

    delay = m_ui.autoscrollBox->value();
    newConfig.delay = delay;

    emit applyConfigTriggered(newConfig);
}

void ConfigForm::restoreConfig()
{
    m_ui.redSliderT->setValue(0);
    m_ui.greenSliderT->setValue(0);
    m_ui.blueSliderT->setValue(0);
    m_ui.redSliderB->setValue(255);
    m_ui.greenSliderB->setValue(255);
    m_ui.blueSliderB->setValue(255);
    m_ui.fontComboBox->setCurrentFont(QFont("Sans"));
    m_ui.sizeSpinBox->setValue(12);
}

void ConfigForm::connectSignals()
{
    connect(m_ui.redSliderT, SIGNAL(valueChanged(int)), this, SLOT(updatePreview()));
    connect(m_ui.greenSliderT, SIGNAL(valueChanged(int)), this, SLOT(updatePreview()));
    connect(m_ui.blueSliderT, SIGNAL(valueChanged(int)), this, SLOT(updatePreview()));
    connect(m_ui.redSliderB, SIGNAL(valueChanged(int)), this, SLOT(updatePreview()));
    connect(m_ui.greenSliderB, SIGNAL(valueChanged(int)), this, SLOT(updatePreview()));
    connect(m_ui.blueSliderB, SIGNAL(valueChanged(int)), this, SLOT(updatePreview()));
    connect(m_ui.fontComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(updatePreview()));
    connect(m_ui.sizeSpinBox, SIGNAL(valueChanged(int)), this, SLOT(updatePreview()));
    // connect other controls
    connect(m_ui.defaultsButton, SIGNAL(clicked()), this, SLOT(restoreConfig()));
    connect(m_ui.applyButton, SIGNAL(clicked()), this, SLOT(applyConfig()));
}
