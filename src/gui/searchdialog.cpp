#include "gui/searchdialog.h"

SearchDialog::SearchDialog(QWidget *parent)
    : QWidget(parent)
{
    m_ui.setupUi(this);
    connectSignals();
}

void SearchDialog::searchForward()
{
    QTextDocument::FindFlags searchFlags;
    if (m_ui.caseBox->isChecked())
        searchFlags |= QTextDocument::FindCaseSensitively;
    if (m_ui.wholeBox->isChecked())
        searchFlags |= QTextDocument::FindWholeWords;
    emit searchTriggered(m_ui.keywordEdit->text(), searchFlags);
}

void SearchDialog::searchBackward()
{
    QTextDocument::FindFlags searchFlags;
    searchFlags |= QTextDocument::FindBackward;
    if (m_ui.caseBox->isChecked())
        searchFlags |= QTextDocument::FindCaseSensitively;
    if (m_ui.wholeBox->isChecked())
        searchFlags |= QTextDocument::FindWholeWords;
    emit searchTriggered(m_ui.keywordEdit->text(), searchFlags);
}

void SearchDialog::connectSignals()
{
    connect(m_ui.forwardButton, SIGNAL(clicked()), this, SLOT(searchForward()));
    connect(m_ui.backwardButton, SIGNAL(clicked()), this, SLOT(searchBackward()));
}
