#ifndef CONTENTSDIALOG_H
#define CONTENTSDIALOG_H

#include "ui_contentsdialog.h"

class ContentsDialog : public QWidget
{
    Q_OBJECT
    
public:
    explicit ContentsDialog(QWidget *parent = 0);

    void showContents(const QStringList &contentsList);
    
signals:
    void markSelectionTriggered(const QString keyWord);

private slots:
    void markSelected(QListWidgetItem *bookmark);

private:
    void connectSignals();

    Ui::ContentsDialog m_ui;
};

#endif // CONTENTSDIALOG_H
