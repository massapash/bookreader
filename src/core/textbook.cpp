#include <QtCore/QTextStream>
#include <QtCore/QStringList>
#include <QtCore/QFile>
#include "core/textbook.h"

TextBook::TextBook()
{
}

QStringList TextBook::loadContents() const
{
    QFile markFile(m_fileName + ".mrk");
    QStringList contentsList;
    markFile.open(QIODevice::ReadOnly);
    if (markFile.isReadable()) {
        QTextStream textStream(&markFile);
        QString textString = textStream.readAll();
        contentsList = textString.split("\n[QUOTE]\n");
    } else {
        contentsList.append(tr("[CANT READ FILE]"));
    }
    markFile.close();
    return contentsList;
}

qint64 TextBook::saveMark(const QString &quoteText) const
{
    QFile markFile(m_fileName + ".mrk");
    markFile.open(QIODevice::ReadWrite);
    if (markFile.isWritable()) {
        QTextStream textStream(&markFile);
        QString textString;
        if (textStream.readAll().isEmpty()) {
            textString = quoteText;
        } else {
            textString = "\n[QUOTE]\n" + quoteText;
        }
        textStream << textString;
    }
    markFile.close();
    return markFile.size();
}
