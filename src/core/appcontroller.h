/**
  AppController -- class to connect gui with model (MVC)
*/

#ifndef APPCONTROLLER_H
#define APPCONTROLLER_H

#include <QtCore/QObject>
#include "core/textbook.h"
#include "core/configbook.h"
#include "gui/bookwindow.h"

class AppController : public QObject
{
    Q_OBJECT

public:
    explicit AppController(QObject *parent = 0);
    ~AppController();

    void exec();

private slots:
    void openBook(const QString &fileName) const;
    void saveMark(const QString &quoteText) const;
    void loadContents() const;
    void loadConfig() const;
    void saveConfig() const;

private:
    void connectSignals() const;

    TextBook *m_textBook;
    ConfigBook *m_configBook;
    BookWindow *m_bookWindow;
};

#endif // APPCONTROLLER_H
