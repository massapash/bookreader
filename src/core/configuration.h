#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <QtGui/QColor>
#include <QtGui/QFont>

struct Configuration
{
    QColor background;
    QColor text;
    QString texture;
    QFont font;
    uint delay;
};

#endif // CONFIGURATION_H
