/**
  ConfigBook -- class envelope over Book. Works with
  config file.
*/

#ifndef CONFIGBOOK_H
#define CONFIGBOOK_H

#include "core/book.h"
#include "core/configuration.h"

class ConfigBook : public Book
{
public:
    ConfigBook();

    Configuration parseConfig();
    qint64 saveConfig(const Configuration &newConfig);

private:
    qint64 restoreConfig();
};

#endif // CONFIGBOOK_H
