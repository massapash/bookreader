/**
  Book -- base class for all classes of book files.
  It contains methods to open and load files.
*/

#ifndef BOOK_H
#define BOOK_H

#include <QtCore/QObject>

class Book : public QObject
{
    Q_OBJECT

public:
    explicit Book(QObject *parent = 0);
    ~Book();

    qint64 loadText(const QString &fileName);
    QString readText() const;

protected:
    QString m_fileName;
    QString m_textString;
};

#endif // BOOK_H
