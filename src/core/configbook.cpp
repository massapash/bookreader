#include <QtCore/QTextStream>
#include <QtCore/QFile>
#include <QDebug>
#include "core/configbook.h"

ConfigBook::ConfigBook()
{
    m_fileName = "bookreader.conf";
}

Configuration ConfigBook::parseConfig() //make more checks
{
    Configuration config;
    while (loadText(m_fileName) == -1)
        restoreConfig();
    if (m_textString.isEmpty())
        restoreConfig();
    QStringList paramsList;
    paramsList = m_textString.split("\n");
    foreach (QString param, paramsList) {
        if (param.contains("background=")) {
            param.remove("background=");
            qDebug() << param;
            QStringList colorList = param.split(",");
            config.background.setRed(colorList.at(0).toInt());
            config.background.setGreen(colorList.at(1).toInt());
            config.background.setBlue(colorList.at(2).toInt());
        }
        if (param.contains("text=")) {
            param.remove("text=");
            qDebug() << param;
            QStringList colorList = param.split(",");
            config.text.setRed(colorList.at(0).toInt());
            config.text.setGreen(colorList.at(1).toInt());
            config.text.setBlue(colorList.at(2).toInt());
        }
        if (param.contains("font=")) {
            param.remove("font=");
            qDebug() << param;
            config.font.setFamily(param);
        }
        if (param.contains("size=")) {
            param.remove("size=");
            qDebug() << param;
            config.font.setPointSize(param.toInt());
        }
        if (param.contains("delay=")) {
            param.remove("delay=");
            qDebug() << param;
            config.delay = param.toInt();
        }
        if (param.contains("texture=")) {
            param.remove("texture=");
            qDebug() << param;
            config.texture = param;
        }
    }
    return config;
}

qint64 ConfigBook::saveConfig(const Configuration &newConfig)
{
    QString configString;
    QString red;
    QString green;
    QString blue;
    QString texture;
    QString font;
    QString size;
    QString delay;

    red = QString::number(newConfig.background.red());
    green = QString::number(newConfig.background.green());
    blue = QString::number(newConfig.background.blue());
    configString.append("background=" + red + "," + green + "," + blue + "\n");

    red = QString::number(newConfig.text.red());
    green = QString::number(newConfig.text.green());
    blue = QString::number(newConfig.text.blue());
    configString.append("text=" + red + "," + green + "," + blue + "\n");

    texture = newConfig.texture;
    configString.append("texture=" + texture + "\n");

    font = newConfig.font.family();
    configString.append("font=" + font + "\n");

    size = QString::number(newConfig.font.pointSize());
    configString.append("size=" + size + "\n");

    delay = QString::number(newConfig.delay);
    configString.append("delay=" + delay);

    m_textString.clear();
    m_textString.append(configString);
    QFile configFile(m_fileName);
    if (configFile.exists()) {
        configFile.remove();
    }
    configFile.open(QIODevice::WriteOnly);
    if (configFile.isWritable()) {
        QTextStream textStream(&configFile);
        textStream << configString;
    } else {
        return -1;
    }
    configFile.close();
    return configFile.size();
}

qint64 ConfigBook::restoreConfig()
{
    Configuration newConfig;
    newConfig.background.setRgb(255,255,255);
    newConfig.text.setRgb(0,0,0);
    newConfig.texture = "";
    newConfig.font.setFamily("Sans");
    newConfig.font.setPointSize(12);
    newConfig.delay = 2000;
    return saveConfig(newConfig);
}
