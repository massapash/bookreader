#include <QtCore/QTextStream>
#include <QtCore/QFile>
#include "core/book.h"

Book::Book(QObject *parent)
    : QObject(parent)
{
}

Book::~Book()
{
}

qint64 Book::loadText(const QString &fileName)
{
    QFile textFile(fileName);
    if (textFile.exists()) {
        textFile.open(QIODevice::ReadOnly);
        if (textFile.isReadable()) {
            QTextStream textStream(&textFile);
            m_textString = textStream.readAll();
        } else {
            m_textString = tr("[CANT READ FILE]");
        }
    } else {
        m_textString = tr("[CANT OPEN FILE]");
        return -1;
    }
    textFile.close();
    m_fileName = fileName;
    return textFile.size();
}

QString Book::readText() const
{
    return m_textString;
}
