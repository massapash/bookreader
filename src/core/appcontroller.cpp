#include "core/appcontroller.h"

AppController::AppController(QObject *parent)
    : QObject(parent)
{
    m_textBook = new TextBook();
    m_configBook = new ConfigBook();
    m_bookWindow = new BookWindow();
    connectSignals();
}

AppController::~AppController()
{
    delete m_bookWindow;
    delete m_textBook;
    delete m_configBook;
}

void AppController::exec()
{
    m_bookWindow->show();

}

void AppController::openBook(const QString &fileName) const
{
    m_textBook->loadText(fileName);
    m_bookWindow->showBook(m_textBook->readText());
}

void AppController::saveMark(const QString &quoteText) const
{
    m_textBook->saveMark(quoteText);
}

void AppController::loadContents() const
{
    m_bookWindow->showContents(m_textBook->loadContents());
}

void AppController::loadConfig() const
{
    m_bookWindow->applyConfig(m_configBook->parseConfig());
}

void AppController::saveConfig() const
{
    m_configBook->saveConfig(m_bookWindow->getConfig());
}

void AppController::connectSignals() const
{
    connect(m_bookWindow, SIGNAL(openBookTriggered(QString)), this, SLOT(openBook(QString)));
    connect(m_bookWindow, SIGNAL(saveMarkTriggered(QString)), this, SLOT(saveMark(QString)));
    connect(m_bookWindow, SIGNAL(loadContentsTriggered()), this, SLOT(loadContents()));
    connect(m_bookWindow, SIGNAL(loadConfigTriggered()), this, SLOT(loadConfig()));
    connect(m_bookWindow, SIGNAL(saveConfigTriggered()), this, SLOT(saveConfig()));
}
