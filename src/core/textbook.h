/**
  TextBook -- class envelope over Book. Works with
  book files.
*/

#ifndef TEXTBOOK_H
#define TEXTBOOK_H

#include "core/book.h"

class TextBook : public Book
{
public:
    TextBook();

    QStringList loadContents() const;
    qint64 saveMark(const QString &quoteText) const;
};

#endif // TEXTBOOK_H
