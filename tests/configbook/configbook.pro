QT += core testlib
QT -= gui
CONFIG += console
CONFIG += test
CONFIG -= app_bundle
TARGET = tst_configbook
TEMPLATE = app
QMAKE_POST_LINK = ./$$TARGET

INCLUDEPATH += ../../src
DEPENDPATH += ../../src

SOURCES += \
    tst_configbook.cpp \
    ../../src/core/book.cpp \
    ../../src/core/configbook.cpp
    
    HEADERS += \
    ../../src/core/book.h \
    ../../src/core/configbook.h
