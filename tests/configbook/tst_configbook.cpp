#include <QtTest/QtTest>
#include "core/configbook.h"

namespace {
    const QString BookReaderConfPath = "configbookdata/bookreader.conf";
    const QString TestConfPath = "configbookdata/test.conf";
} // anonymous namespace

class ConfigBookTest : public QObject
{
    Q_OBJECT

public:
    ConfigBookTest();

private slots:
    void initTestCase();
    void cleanupTestCase();
    void init();
    void cleanup();
    void saveText();

private:
    ConfigBook *m_configBook;
    QFile m_testFile;

    QString readFile(const QString &fileName);
    void writeFile(const QString &fileName, const QString &data);
    void deleteFile(const QString &fileName);
};

ConfigBookTest::ConfigBookTest()
{
}

void ConfigBookTest::initTestCase()
{
}

void ConfigBookTest::init()
{
    m_configBook = new ConfigBook();
    m_configBook->loadText(BookReaderConfPath);
}

void ConfigBookTest::cleanup()
{
    delete m_configBook;
}

void ConfigBookTest::cleanupTestCase()
{
    deleteFile(TestConfPath);
}

QString ConfigBookTest::readFile(const QString &fileName)
{
    QString testString;
    QTextStream testStream;
    m_testFile.setFileName(fileName);
    if (m_testFile.exists()) {
        m_testFile.open(QIODevice::ReadOnly);
        if (m_testFile.isReadable()) {
            testStream.setDevice(&m_testFile);
            testString = testStream.readAll();
        } else {
            testString = "[CANT READ FILE]";
        }
    } else {
        testString = "[CANT OPEN FILE]";
    }
    m_testFile.close();
    return testString;
}

void ConfigBookTest::writeFile(const QString &fileName, const QString &data)
{
    QTextStream testStream;
    m_testFile.setFileName(fileName);
    m_testFile.open(QIODevice::WriteOnly);
    if (m_testFile.isWritable()) {
        testStream.setDevice(&m_testFile);
        testStream << data;
    } else {
        m_testFile.close();
        return;
    }
    m_testFile.close();
}

void ConfigBookTest::deleteFile(const QString &fileName)
{
    m_testFile.setFileName(fileName);
    m_testFile.open(QIODevice::WriteOnly);
    if (m_testFile.isWritable()) {
        m_testFile.remove();
    } else {
        m_testFile.close();
        return;
    }
    m_testFile.close();
}

void ConfigBookTest::saveText()
{

    QString testString = readFile(BookReaderConfPath);
    testString.append("\ntxcolor=0xffffff");
    testString.append("\nbkcolor=0x000000");
    writeFile(TestConfPath, testString);
    QCOMPARE(m_configBook->saveText(testString), m_testFile.size());
}

QTEST_APPLESS_MAIN(ConfigBookTest)
#include "tst_configbook.moc"
