#include <QtTest/QtTest>
#include "core/textbook.h"

class TextBookTest : public QObject
{
    Q_OBJECT

public:
    TextBookTest();

private slots:
    void initTestCase();
    void cleanupTestCase();
    void init();
    void cleanup();
    void findText();
    void saveMark();

private:
    TextBook *m_textBook;
    QFile m_testFile;

    QString readFile(const QString &fileName);
    void writeFile(const QString &fileName, const QString &data);
    void deleteFile(const QString &fileName);
};

TextBookTest::TextBookTest()
{
}

void TextBookTest::initTestCase()
{
}

void TextBookTest::init()
{
    m_textBook = new TextBook();
    m_textBook->loadText("../testsdata/test.txt");
}

void TextBookTest::cleanup()
{
    delete m_textBook;
}

void TextBookTest::cleanupTestCase()
{
    deleteFile("textbookdata/test.mrk");
    deleteFile("../testsdata/test.txt.mrk");
}

QString TextBookTest::readFile(const QString &fileName)
{
    QString testString;
    QTextStream testStream;
    m_testFile.setFileName(fileName);
    if (m_testFile.exists()) {
        m_testFile.open(QIODevice::ReadOnly);
        if (m_testFile.isReadable()) {
            testStream.setDevice(&m_testFile);
            testString = testStream.readAll();
        } else {
            testString = "[CANT READ FILE]";
        }
    } else {
        testString = "[CANT OPEN FILE]";
    }
    m_testFile.close();
    return testString;
}

void TextBookTest::writeFile(const QString &fileName, const QString &data)
{
    QTextStream testStream;
    m_testFile.setFileName(fileName);
    m_testFile.open(QIODevice::WriteOnly);
    if (m_testFile.isWritable()) {
        testStream.setDevice(&m_testFile);
        testStream << data;
    } else {
        m_testFile.close();
        return;
    }
    m_testFile.close();
}

void TextBookTest::deleteFile(const QString &fileName)
{
    m_testFile.setFileName(fileName);
    m_testFile.open(QIODevice::WriteOnly);
    if (m_testFile.isWritable()) {
        m_testFile.remove();
    } else {
        m_testFile.close();
        return;
    }
    m_testFile.close();
}

void TextBookTest::findText()
{
    QString testString = readFile("../testsdata/test.txt");
    QString keyWord = "fking test";
    int position = 0;
    position = testString.indexOf(keyWord, position, Qt::CaseInsensitive);
    QCOMPARE(m_textBook->findText(keyWord), position);
}

void TextBookTest::saveMark()
{
    QString testString = "0";
    writeFile("textbookdata/test.mrk", testString);
    QCOMPARE(m_textBook->saveMark(), m_testFile.size());
}

QTEST_APPLESS_MAIN(TextBookTest)
#include "tst_textbook.moc"
