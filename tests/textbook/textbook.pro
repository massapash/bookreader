QT += core testlib
QT -= gui
CONFIG += console
CONFIG += test
CONFIG -= app_bundle
TARGET = tst_textbook
TEMPLATE = app
QMAKE_POST_LINK = ./$$TARGET

INCLUDEPATH += ../../src
DEPENDPATH += ../../src

SOURCES += \
    tst_textbook.cpp \
    ../../src/core/book.cpp \
    ../../src/core/textbook.cpp
    
    HEADERS += \
    ../../src/core/book.h \
    ../../src/core/textbook.h
