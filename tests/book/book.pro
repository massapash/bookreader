QT += core testlib
QT -= gui
CONFIG += console
CONFIG += test
CONFIG -= app_bundle
TARGET = tst_book
TEMPLATE = app
QMAKE_POST_LINK = ./$$TARGET

INCLUDEPATH += ../../src
DEPENDPATH += ../../src

SOURCES += \
    tst_book.cpp \
    ../../src/core/book.cpp

HEADERS += \
    ../../src/core/book.h
