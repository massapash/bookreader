#include <QtTest/QtTest>
#include "core/book.h"

class BookTest : public QObject
{
    Q_OBJECT

public:
    BookTest();

private slots:
    void initTestCase();
    void cleanupTestCase();
    void init();
    void cleanup();
    void loadText();
    void readText();

private:
    Book *m_book;
    QFile m_testFile;

    QString readFile(const QString &fileName);
};

BookTest::BookTest()
{
}

void BookTest::initTestCase()
{
}

void BookTest::init()
{
    m_book = new Book();
    m_book->loadText("../testsdata/test.txt");
}

QString BookTest::readFile(const QString &fileName)
{
    QString testString;
    QTextStream testStream;
    m_testFile.setFileName(fileName);
    if (m_testFile.exists()) {
        m_testFile.open(QIODevice::ReadOnly);
        if (m_testFile.isReadable()) {
            testStream.setDevice(&m_testFile);
            testString = testStream.readAll();
        } else {
            testString = "[CANT READ FILE]";
        }
    } else {
        testString = "[CANT OPEN FILE]";
    }
    m_testFile.close();
    return testString;
}

void BookTest::cleanup()
{
    delete m_book;
}

void BookTest::cleanupTestCase()
{
}



void BookTest::loadText()
{
    m_testFile.setFileName("../testsdata/test.txt");
    if (m_testFile.exists()) {
        m_testFile.open(QIODevice::ReadOnly);
    } else {
        return;
    }
    m_testFile.close();
    QCOMPARE(m_book->loadText("../testsdata/test.txt"), m_testFile.size());
}

void BookTest::readText()
{
    QString testString = readFile("../testsdata/test.txt");
    QCOMPARE(m_book->readText(), testString);
}

QTEST_APPLESS_MAIN(BookTest)
#include "tst_book.moc"
