QT += core gui
TARGET = bookreader
TEMPLATE = app

INCLUDEPATH = src
DEPENDPATH = src

SOURCES += \
    main.cpp \
    core/book.cpp \
    core/textbook.cpp \
    core/configbook.cpp \
    core/appcontroller.cpp \
    gui/bookwindow.cpp \
    gui/configform.cpp \
    gui/searchdialog.cpp \
    gui/contentsdialog.cpp

HEADERS += \
    core/book.h \
    core/textbook.h \
    core/configbook.h \
    core/appcontroller.h \
    gui/bookwindow.h \
    gui/configform.h \
    gui/searchdialog.h \
    gui/contentsdialog.h \
    core/configuration.h

FORMS += \
    gui/bookwindow.ui \
    gui/configform.ui \
    gui/searchdialog.ui \
    gui/contentsdialog.ui

RESOURCES += \
    gui/images.qrc
